package service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import service.impl.FileHtmlImpl;
import util.FilePath;

import java.io.File;
import java.time.LocalDateTime;

@RunWith(BlockJUnit4ClassRunner.class)
public class FileHtmlImplTest {

    private static final String URL = "https://simbirsoft.com/";
    private static final String FILE_PATH = FilePath.DISK + "TEST" + FilePath.HTML;
    private static final FileHtml fileHtml = new FileHtmlImpl();

    @Test
    public void downloadTest() {
        fileHtml.download(URL, FILE_PATH);
        File file = new File(FILE_PATH);

        Assert.assertTrue(file.exists());
    }

    @After
    public void deleteFile() {
        new File(FILE_PATH).delete();
    }
}
