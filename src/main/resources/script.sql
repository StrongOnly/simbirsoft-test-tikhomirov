-- Database: websites

CREATE DATABASE websites
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Table: public.words

CREATE TABLE public.words
(
    id integer NOT NULL DEFAULT nextval('words_id_seq'::regclass),
    word character varying(255) COLLATE pg_catalog."default" NOT NULL,
    amount bigint NOT NULL,
    site character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT words_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.words
    OWNER to postgres;