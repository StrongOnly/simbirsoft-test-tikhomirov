package util;

import entity.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WordMapper {
    /**
     *convects map to list consisting of Word
     */
    public static List<Word> mapToList(Map<String, Long> map, String url) {
        List<Word> wordList = new ArrayList<>();
        for (Map.Entry<String, Long> stringLongEntry : map.entrySet()) {
            Word word = new Word();
            word.setWord(stringLongEntry.getKey());
            word.setAmount(stringLongEntry.getValue());
            word.setSite(url);
            wordList.add(word);
        }
        return wordList;
    }
}
