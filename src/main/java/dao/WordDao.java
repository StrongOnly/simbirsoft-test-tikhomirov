package dao;

import entity.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class WordDao {
    private static final String SELECT_MAX_ID_QUERY = "SELECT max(id) FROM words";
    private static final String INSERT_INTO_QUERY = "INSERT INTO words VALUES (?, ?, ?, ?)";
    private static final String SELECT_WORDS_BY_WORD_SITE_QUERY = "SELECT * FROM words WHERE word = ? AND site = ?";
    private static final String UPDATE_AMOUNT_BY_ID_QUERY = "UPDATE words SET amount = ? WHERE id = ?";
    private static final Logger logger = LoggerFactory.getLogger(WordDao.class);

    /**
     *adds new words to the database
     */
    public boolean addWords(List<Word> wordList) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            long id = getMaxIdFromDatabase() + 1;
            for (Word word : wordList) {
                if (findByWordAndSite(word.getWord(), word.getSite()) != null) {
                    updateWords(word);
                } else {
                    PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_QUERY);
                    preparedStatement.setLong(1, id++);
                    preparedStatement.setString(2, word.getWord());
                    preparedStatement.setLong(3, word.getAmount());
                    preparedStatement.setString(4, word.getSite());
                    preparedStatement.execute();
                }
            }
            return true;
        } catch (SQLException e) {
            logger.error("Unable to connect to database");
            return false;
        }
    }

    private Long getMaxIdFromDatabase() {
        long maxId = -1;
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_MAX_ID_QUERY);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            maxId = rs.getLong(1);
        } catch (SQLException e) {
            logger.error("Unable to connect to database");
        }
        return maxId;
    }

    private Word findByWordAndSite(String word, String url) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_WORDS_BY_WORD_SITE_QUERY);
            preparedStatement.setString(1, word);
            preparedStatement.setString(2, url);
            ResultSet resultSet = preparedStatement.getResultSet();
            if (resultSet == null) {
                return null;
            }
            long resultId = resultSet.getLong("id");
            String resultWord = resultSet.getString("word");
            long resultAmount = resultSet.getLong("amount");
            String resultSite = resultSet.getString("site");
            return new Word(resultId, resultWord, resultAmount, resultSite);
        } catch (SQLException e) {
            logger.error("Unable to connect to database");
            return null;
        }
    }

    private boolean updateWords(Word word) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AMOUNT_BY_ID_QUERY);
            preparedStatement.setLong(1, word.getAmount());
            preparedStatement.setLong(2, word.getId());
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            logger.error("Unable to connect to database");
            return false;
        }
    }
}
