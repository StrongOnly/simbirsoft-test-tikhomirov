package entity;

public class Word {
    private long id;
    private String word;
    private long amount;
    private String site;

    public Word() {

    }

    public Word(long id, String word, long amount, String site) {
        this.id = id;
        this.word = word;
        this.amount = amount;
        this.site = site;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }


}
