
import dao.WordDao;
import entity.Word;
import service.FileHtml;
import service.HtmlParser;
import service.TextSort;
import service.impl.TextSortImpl;
import service.impl.FileHtmlImpl;
import service.impl.HtmlParserImpl;
import util.FilePath;
import util.WordMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {

    private static final FileHtml saveFile = new FileHtmlImpl();
    private static final HtmlParser parser = new HtmlParserImpl();
    private static final TextSort sort = new TextSortImpl();
    private static final WordDao wordDao = new WordDao();
    private static final Scanner scanner = new Scanner(System.in);
//    private static final String url1 = "https://www.simbirsoft.com/";
//    private static final String url2 = "https://javarush.ru/";
//    private static final String url3 = "https://www.youtube.com/";

    public static void main(String[] args) {
        String url = scanner.nextLine();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss_SS");
        String filePath = FilePath.DISK + LocalDateTime.now().format(dateTimeFormatter) + FilePath.HTML;
        saveFile.download(url, filePath);
        String text = parser.getTextFromHtml(url);
        Map<String, Long> map = sort.textSort(text);
        List<Word> list = WordMapper.mapToList(map, url);
        wordDao.addWords(list);

//        saveFile.download(url, filePath);
//        text = parser.getTextFromHtml(url1);
//        map = sort.textSort(text);
//        list = WordMapper.mapToList(map, url1);
//        wordDao.addWords(list);
//
//        saveFile.download(url, filePath);
//        text = parser.getTextFromHtml(url2);
//        map = sort.textSort(text);
//        list = WordMapper.mapToList(map, url2);
//        wordDao.addWords(list);
//
//        saveFile.download(url, filePath);
//        text = parser.getTextFromHtml(url3);
//        map = sort.textSort(text);
//        list = WordMapper.mapToList(map, url3);
//        wordDao.addWords(list);
    }
}
