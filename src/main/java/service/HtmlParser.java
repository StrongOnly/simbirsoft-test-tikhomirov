package service;


public interface HtmlParser {
    /**
     * gets text from result body
     */
    String getTextFromHtml(String url);
}
