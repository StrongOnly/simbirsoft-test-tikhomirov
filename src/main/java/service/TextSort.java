package service;

import java.util.Map;

public interface TextSort {
    /**
     * removes all characters that are considered delimiters from text
     * counts how many times identical words are found in text
     * prints the result to the console
     */
    Map<String, Long> textSort(String text);
}
