package service;

public interface FileHtml {
    /**
     * downloads file from chosen url
     */
    void download(String url, String filePath);
}
