package service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.FilePath;
import service.FileHtml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class FileHtmlImpl implements FileHtml {
    private static final Logger logger = LoggerFactory.getLogger(FileHtmlImpl.class);

    @Override
    public void download(String url, String filePath) {
        try (InputStream in = URI.create(url).toURL().openStream()) {
            Files.copy(in, Paths.get(filePath));
        } catch (IOException e) {
            logger.error("Couldn't download html " + url);
        }
    }
}
