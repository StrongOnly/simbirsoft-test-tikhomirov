package service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.HtmlParser;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HtmlParserImpl implements HtmlParser {
    private static final Logger logger = LoggerFactory.getLogger(HtmlParserImpl.class);

    @Override
    public String getTextFromHtml(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            logger.error("Unable to connect to url");
        }
        String text = doc.body().text();
        return text;
    }
}
