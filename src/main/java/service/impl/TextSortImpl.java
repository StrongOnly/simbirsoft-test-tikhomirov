package service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.TextSort;

import java.util.*;

public class TextSortImpl implements TextSort {
    private static final Logger logger = LoggerFactory.getLogger(TextSortImpl.class);

    @Override
    public Map<String, Long> textSort(String text) {
        String equally = " = ";
        String[] textArr = text.replaceAll("[ .,!?\":;()\\[\\]\n\r\t]", " ")
                .toLowerCase()
                .trim()
                .split("\\s+");
        Map<String, Long> textMap = new HashMap<>();
        for (String word : textArr) {
            textMap.putIfAbsent(word, 0L);
            textMap.put(word, textMap.get(word) + 1);
        }
        for (Map.Entry<String, Long> entry : textMap.entrySet()) {
            String key = entry.getKey();
            Long value = entry.getValue();
            logger.info(key + equally + value);
        }
        return textMap;
    }
}
